# Humane-Like Color Theme - Change Log

## [0.4.8]

- tweak widget/notification borders and backgrounds

## [0.4.7]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.4.6]

- update readme and screenshot

## [0.4.5]

- fix menu FG/BG
- fix focusborder
- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- define window border in custom mode

## [0.4.4]

- fix titlebar border, FG/BG in custom mode

## [0.4.3]

- fix pub issues on OVSX
- ...

## [0.4.1]

- fix manifest repo links

## [0.4.0]

- make button BG lighter, to reduce how much it competes visually with project names in sidebar source-control view with multi-project workspaces; makes it a little easier to see which project you are commenting / committing. (Ideally we should be abe to make list.background stand out, but I see no such setting.)
- retain v0.3.1 for those who prefer earlier style

## [0.3.1]

- make badge BG transparent
- make list active selection BG transparent

## [0.3.0]

- change button.foreground/background to match badges
- change statusbar.FG/BG to match schema
- retain v0.2.5 for those who prefer earlier style

## [0.2.5]

- soften editor.lineHighlightBorder" to "#a9a9a960"

## [0.2.4]

- fix syntax FG contrast on exceptional cases:
illegal
Broken
Deprecated
Unimplemented

## [0.2.3]

- fix scrollbar, minimap slider transparencies
"minimapSlider.activeBackground": "#00000040",
"minimapSlider.background": "#00000020",
"minimapSlider.hoverBackground": "#00000060",
"scrollbarSlider.activeBackground": "#00000040",
"scrollbarSlider.background": "#00000020",
"scrollbarSlider.hoverBackground": "#00000060",
- fix completion list contrast:
editorSuggestWidget.selectedBackground darkened (slightly)
list.hoverBackground OK

## [0.2.2]

- fix badges FG/BG

## [0.2.1]

- fix contrast sidebar BG vs search/replace BG
- additional border contrasts

## [0.2.0]

- retain v0.1.5 for those who prefer earlier style
- make BG colors more consistent
- reverse activity bar active / inactive FG
- lighten borders
- lighten tab BG

## [0.1.5]

- fix contrast of notification, hover widget FG/BG

## [0.1.4]

- hover widget FG/BG

## [0.1.3]

- notification FG/BG

## [0.1.2]

- scrollbar and widget shadows
- lighten background of editor and general panels, for contrast

## [0.1.1]

- fix contrast of list.activeselectionforeground

## [0.1.0]

- update syntax colors similar to humanedark
- use more consistent colors for activity bar, editor widgets (find, command)
- retain most recent older version of the .VSIX for those who prefer, or are accustomed to, the original look.

## [0.0.6]

- scale icon down
- change icon colorspace

## [0.0.5]

- rename publisher and add additional manifest values

## [0.0.4]

- restructure project folder and VSIX contents

## [0.0.3]

- darken top border of code tabs
- darken editor gutter to separate from code background

## [0.0.2]

- lighten list.activeSelectionBackground

## [0.0.1]

- Initial release
