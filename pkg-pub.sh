#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./sjsepan-humanelike-0.4.8.vsix
npx ovsx publish sjsepan-humanelike-0.4.8.vsix --debug --pat <PAT>