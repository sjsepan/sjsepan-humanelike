# Humane-Like Theme

Humane-Like color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-humanelike_code.png](./images/sjsepan-humanelike_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-humanelike_codium.png](./images/sjsepan-humanelike_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-humanelike_codedev.png](./images/sjsepan-humanelike_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-humanelike_ads.png](./images/sjsepan-humanelike_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-humanelike_theia.png](./images/sjsepan-humanelike_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-humanelike_positron.png](./images/sjsepan-humanelike_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/9/2025
